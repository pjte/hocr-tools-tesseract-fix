This repository contains fixes which allows hocr-tools to use Tesseract files.

It includes:

- modified PyXML library: 'as' keyword, problems with html tag in Document.py

(http://sourceforge.net/projects/pyxml/)

- modified hocr-tools: hocr-check, hocr-eval and hocr-eval-lines files

(https://code.google.com/p/hocr-tools/)
